//
//  CharacterData.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import EVReflection
import RealmSwift


class CharacterData: EVObject{
    
    var count : Int = 0
    var limit : Int = 0
    var offset : Int = 0
    var results : [Result] = [Result] ()
    var total : Int =  0
    

}
