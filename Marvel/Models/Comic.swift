//
//  Comic.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//


import Foundation
import EVReflection
import RealmSwift


class Comic: EVObject{
    
    var available : Int = 0
    var collectionURI : String = ""
    var items : [String] = [""]
    var returned : Int = 0
    
    
}
