//
//  Reasult.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import EVReflection
import RealmSwift

class Result: EVObject{
    
    var comics : Comic = Comic()
    var Description : String = ""
    var events : Comic = Comic()
    var id : String = ""
    var modified : String = ""
    var name : String = ""
    var resourceURI : String = ""
    var series : Comic = Comic()
    var stories : Comic = Comic()
    var thumbnail : Thumbnail = Thumbnail()
    var urls : [Url] = []
    

    
}

extension Result {
    enum CodingKeys: String, CodingKey {
        case Description = "description"

    }
}
