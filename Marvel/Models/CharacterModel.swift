//
//  CharacterModel.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import EVReflection
import RealmSwift


class ChracterRootClass: EVObject {
    
    var attributionHTML : String = ""
    var attributionText : String = ""
    var code : Int = 0
    var copyright : String = ""
    var data : CharacterData = CharacterData()
    var etag : String  = ""
    var status : String  = ""
    

    
}
