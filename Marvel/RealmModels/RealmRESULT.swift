//
//  RealmRESULT.swift
//  Marvel
//
//  Created by Adam on 10/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift

class RealmRESULT: Object {
    @objc  dynamic var comic: RealmComic? = nil
    @objc  dynamic var descriptionField: String = ""
    @objc  dynamic var event: RealmComic? = nil
    @objc  dynamic var id: String = String(UUID.init().hashValue)
    @objc  dynamic var modified: String = ""
    @objc  dynamic var name: String = ""
    @objc  dynamic var resourceURI: String = ""
    @objc  dynamic var series: RealmComic? = nil
    @objc  dynamic var story: RealmComic? = nil
    @objc  dynamic var thumbnail: RealmThumbnail? = nil
     let urls : List<RealmUrl> = List<RealmUrl>()
    
    static func newRealmChracterRootClass ( from roots :[Result] ) -> [RealmRESULT] {
        var restult : [RealmRESULT] = []
        
        for root in roots {
        let newObject = RealmRESULT()
        newObject.Id = root.id
        newObject.DescriptionField = root.Description
        newObject.Modified = root.modified
        newObject.Name = root.name
        let comicsObject = RealmComic.newRealmChracterRootClass(from: root.comics)
        newObject.Comic = comicsObject
        let seriesObject = RealmComic.newRealmChracterRootClass(from: root.series)
        newObject.Comic = seriesObject
        let eventsObject = RealmComic.newRealmChracterRootClass(from: root.events)
        newObject.Comic = eventsObject
        let storiesObject = RealmComic.newRealmChracterRootClass(from: root.stories)
        newObject.Comic = storiesObject
          restult.append(newObject)
        }
        
        return restult
    }
    
    ///opthinal example
    
    var DescriptionField: String {
        get{
            return descriptionField
        }
        set{
            safeWrite {
                descriptionField = newValue
            }
        }
    }

    var Id: String {
        get{
            return id
        }
        set{
            safeWrite {
                id = newValue
            }
        }
    }
    var Modified: String {
        get{
            return modified
        }
        set{
            safeWrite {
                modified = newValue
            }
        }
    }
    var Name: String {
        get{
            return name
        }
        set{
            safeWrite {
                name = newValue
            }
        }
    }
    var ResourceURI: String {
        get{
            return resourceURI
        }
        set{
            safeWrite {
                resourceURI = newValue
            }
        }
    }
    
    
    
    
    var Event: RealmComic? {
        get{
            return event
        }
        set{
            safeWrite {
                event = newValue
            }
            
        }
    }
    
    var Series: RealmComic? {
        get{
            return series
        }
        set{
            safeWrite {
                series = newValue
            }
            
        }
    }
    
    var Comic: RealmComic? {
        get{
            return comic
        }
        set{
            safeWrite {
                comic = newValue
            }
            
        }
    }
   
    var Story: RealmComic? {
        get{
            return story
        }
        set{
            safeWrite {
                story = newValue
            }
            
        }
    }
    
    var Urls: [RealmUrl] {
        get{
            return Array(urls)
        }
        set{
            safeWrite {
                urls.append(objectsIn: newValue)
            }
        }
        
    }
    
    func addUrl(url : RealmUrl) {
        safeWrite {
            urls.append(url)
        }
    }
}
