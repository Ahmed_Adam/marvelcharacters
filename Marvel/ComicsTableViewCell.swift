//
//  ComicsTableViewCell.swift
//  Marvel
//
//  Created by Adam on 10/13/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class ComicsTableViewCell: UICollectionViewCell {

    @IBOutlet weak var comicImage: UIView!
    @IBOutlet weak var comicName: UILabel!
    

}
