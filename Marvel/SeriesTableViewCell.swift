//
//  SeriesTableViewCell.swift
//  Marvel
//
//  Created by Adam on 10/13/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class SeriesTableViewCell: UICollectionViewCell {

    @IBOutlet weak var seriesImage: UIView!
    @IBOutlet weak var seriesName: UILabel!
    

}
