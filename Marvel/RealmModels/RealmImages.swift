
//  RealmImages.swift
//  Marvel
//
//  Created by Adam on 10/17/18.
//  Copyright © 2018 Adam. All rights reserved.

import Foundation
import  RealmSwift

class RealmImages: Object{
    
    @objc dynamic var Image :NSData? = nil
    
    static func newRealmImage ( from image :UIImage  ) -> RealmImages {
        let newObject = RealmImages()
        let data = NSData(data: UIImageJPEGRepresentation(image,0.9)!)
        let imgPNG = UIImage(data: data as Data)
        let dataPNGImg = NSData(data: UIImagePNGRepresentation(imgPNG!)!)
        newObject.Image = dataPNGImg
        
        return newObject
    }
    
}
