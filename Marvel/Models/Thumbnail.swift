//
//  Thumbnail.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import EVReflection
import RealmSwift

class Thumbnail: EVObject{
    
    var Extension : String = ""
    var path : String = ""
    
    
}
extension Thumbnail {
    enum CodingKeys: String, CodingKey {
        case Extension = "extension"
        
    }
}
