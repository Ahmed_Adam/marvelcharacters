//
//  WebService.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//


import Foundation
import Alamofire
import SwiftyJSON
import AlamofireImage

class WebServices{
    
    static let base = WebServices()
//    http://gateway.marvel.com/v1/public/characters?ts=1&apikey=9e965f7ed63130d311a564db28b806ac&hash=f5c4c0aa5c783d02edd062a7ae2f88b3
    let  baseUrl: String = "http://gateway.marvel.com/v1/public/"
    let ts = "1"
    let apikey = "9e965f7ed63130d311a564db28b806ac"
    let hash = "f5c4c0aa5c783d02edd062a7ae2f88b3"
    

    
    
    
    func GetMethod (requestUrl : String , complition :   @escaping (_ error:Error? ,_ success: Bool , _ response  : String) -> Void){
        
        
        let urlwithPercentEscapes = requestUrl 
        
        let url = URL(string : urlwithPercentEscapes)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            complition(nil, true, jsonString!)
        }
    }
    
    
    func getPhoto (url: String , complition :   @escaping (_ error:Error? ,_ success: Bool , _ photo: UIImage ) -> Void){
        let  imageUrl =  url
        Alamofire.request(imageUrl, method: .get).responseImage { response in
            print(response)
            guard let image = response.result.value else {
                // Handle error
                
                return
            }
            // Do stuff with your image
            complition(nil,true , image)
        }
        
    }
    
}
extension String
{
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
    }
    
    
}
