//
//  RealmCharacterData.swift
//  Marvel
//
//  Created by Adam on 10/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift


class RealmCharacterData: Object{
    @objc  dynamic var count: Int = 0
    @objc  dynamic var limit: Int = 0
    @objc  dynamic var offset: Int = 0
    @objc  dynamic var total: Int = 0
     let results: List<RealmRESULT> = List<RealmRESULT>()
    
   static func newRealmChracterRootClass ( from root :CharacterData ) -> RealmCharacterData {
        let newObject = RealmCharacterData()
        newObject.Count = root.count
        newObject.Limit = root.limit
        newObject.Offset = root.offset
        newObject.total = root.total
        let realmObject = RealmRESULT.newRealmChracterRootClass(from: root.results)
        newObject.Results = realmObject
    
        
        return newObject
    }
    
    var Results: [RealmRESULT] {
        get{
            return Array(results)
        }
        set{
            safeWrite {
                results.append(objectsIn: newValue)
            }
        }
        
    }
    
    var Count: Int {
        get{
            return count
        }
        set{
            safeWrite {
                count = newValue
            }
        }
        
    }
    var Limit: Int {
        get{
            return limit
        }
        set{
            safeWrite {
                limit = newValue
            }
        }
        
    }
    var Offset: Int {
        get{
            return offset
        }
        set{
            safeWrite {
                offset = newValue
            }
        }
        
    }
}
