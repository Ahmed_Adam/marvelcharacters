//
//  Realm ChracterRootClass.swift
//  Marvel
//
//  Created by Adam on 10/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift


class RealmChracterRootClass: Object {
    @objc  dynamic var attributionHTML: String = ""
    @objc  dynamic var attributionText: String = ""
    @objc  dynamic var code: Int = 0
    @objc  dynamic var copyright: String = ""
    @objc  dynamic var data: RealmCharacterData?
    @objc  dynamic var etag: String = ""
    @objc  dynamic var status: String = ""
    
    static func newRealmChracterRootClass ( from root :ChracterRootClass ) -> RealmChracterRootClass {
        let newObject = RealmChracterRootClass()
        newObject.attributionHTML = root.attributionHTML
        newObject.attributionText = root.attributionText
        newObject.code = root.code
        let realmObject = RealmCharacterData.newRealmChracterRootClass(from: root.data)
        newObject.data = realmObject
        newObject.etag = root.etag
        newObject.status = root.status
        
        return newObject
    }
    
//    var Data: [RealmCharacterData] {
//        get{
//            return Array(data)
//        }
//        set{
//            safeWrite {
//                data.append(objectsIn: newValue)
//            }
//        }
//
//    }
    
    var AttributionHTML: String {
        get{
            return attributionHTML
        }
        set{
            safeWrite {
                attributionHTML = newValue
            }
        }
        
    }
    var AttributionText: String {
        get{
            return attributionText
        }
        set{
            safeWrite {
                attributionText = newValue
            }
        }
        
    }
    
    var Code: Int {
        get{
            return code
        }
        set{
            safeWrite {
                code = newValue
            }
        }
        
    }
    
    
    var Copyright: String {
        get{
            return copyright
        }
        set{
            safeWrite {
                copyright = newValue
            }
        }
        
    }
    var Etag: String {
        get{
            return etag
        }
        set{
            safeWrite {
                etag = newValue
            }
        }
        
    }
    var Status: String {
        get{
            return status
        }
        set{
            safeWrite {
                status = newValue
            }
        }
        
    }
    
    
}
