//
//  CharacterCellTableViewCell.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class CharacterCellTableViewCell: UITableViewCell {


    @IBOutlet weak var backGround: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    

}
