//
//  ViewController.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright � 2018 Adam. All rights reserved.
//

import UIKit
import SceneKit
import NVActivityIndicatorView
import Alamofire
import  RealmSwift

class ViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , NVActivityIndicatorViewable{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cancl: UIButton!
    @IBOutlet weak var chracterTableView: UITableView!
    var marvelCharacter : ChracterRootClass?
    var realmCharacters : [RealmChracterRootClass] = []
    var realmImages : [RealmImages] = []
    var marevel_id: String!
    var offline = false
    let reachabilityManager = NetworkReachabilityManager()
    var offset: Int = 0
    var limit: Int = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.offline = false
        loading()
        let check = checkNetwork()
        if check {
            getCHaracters()
        }
        let logo = #imageLiteral(resourceName: "marvel")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    func checkNetwork()-> Bool {
        reachabilityManager?.startListening()
        if  reachabilityManager?.isReachable == false {
            print("The network is not reachable")
            self.stopAnimating()
            let alert = UIAlertController(title: "Alert", message: " The network is not reachable , please make sure of internet connection " , preferredStyle: UIAlertControllerStyle.alert)
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Close application ", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    exit(0)
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            
            alert.addAction(UIAlertAction(title: "wait for connection ... ", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.loading()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
                        self.stopAnimating()
                        let check = self.checkNetwork()
                        if check {
                            self.getCHaracters()
                        }
                    }
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            alert.addAction(UIAlertAction(title: " work offline  ", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.offline = true
                    self.realmCharacters = RealmManager.shared.getObjectOf(type: RealmChracterRootClass.self)
                    self.realmImages = RealmManager.shared.getObjectOf(type: RealmImages.self)
                    self.chracterTableView.isHidden = false
                    self.chracterTableView.reloadData()
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            return false
        }
        return true
    }
    
    // API Requset that returns the CHaracters
    func getCHaracters(){
        let url = "http://gateway.marvel.com/v1/public/characters?ts=1&apikey=9e965f7ed63130d311a564db28b806ac&hash=f5c4c0aa5c783d02edd062a7ae2f88b3"  + "&limit=" + "\(self.limit)"
        WebServices.base.GetMethod(requestUrl: url) { (error, success , response ) in
            self.marvelCharacter = ChracterRootClass(json: response)
            let realmObject = RealmChracterRootClass.newRealmChracterRootClass(from: self.marvelCharacter!)
            RealmManager.shared.addObject(realmObject: realmObject , andCompletion : {
                (addResult) in
                print(addResult)
            } )
            self.chracterTableView.reloadData()
            self.stopAnimating()
            self.chracterTableView.isHidden = false
        }
    }
    
    
    @IBAction func search(_ sender: UIButton) {
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.red
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.white
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.offline == true{
            let count = self.realmCharacters[0].data?.results.count ?? 0
            return count
        }
        if (self.marvelCharacter?.data.results.count) != nil  {
            return self.marvelCharacter?.data.results.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // when no internet connection Load from Realm DB
        if self.offline == true{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CharacterCellTableViewCell
            cell.characterName.text = self.realmCharacters[0].data?.Results[indexPath.row].Name

                 cell.backGround.image = UIImage(data: self.realmImages[indexPath.row].Image! as Data)

           
            return cell
        }
        
        
        //  load Characters from API
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CharacterCellTableViewCell
        cell.characterName.text = self.marvelCharacter?.data.results[indexPath.row].name
        let imageUrl = (self.marvelCharacter?.data.results[indexPath.row].thumbnail.path)! + "." + "jpg"
        WebServices.base.getPhoto(url: imageUrl) { (error , success , image ) in
            cell.backGround.image = image
            let realmImage = RealmImages.newRealmImage(from: image)
            RealmManager.shared.addObject(realmObject: realmImage  , andCompletion : {
                (addResult) in
                print(addResult)
            } )
        }
        
       
        return cell
    }
    
    
    // select one of the Characters
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.offline == true{
            return
        }
        self.marevel_id = self.marvelCharacter?.data.results[indexPath.row].id
        self.performSegue(withIdentifier: "showMarvel", sender: self)
    }
    
    
    // make the Cracters request returned 20 characters more
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.offline == true{
            return
        }
        if indexPath.row ==  self.limit - 1 {
            loading()
            self.offset = self.offset + 20
            self.limit = self.limit + 20
            self.getCHaracters()
        }
    }
    
    
    // Loading
    func loading(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "", type: NVActivityIndicatorType.lineSpinFadeLoader)
    }
    
    // send Character Id by segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMarvel" {
            if let characterViewController = segue.destination as? resultsTableViewController {
                characterViewController.id = self.marevel_id
            }
        }
    }
    
}

