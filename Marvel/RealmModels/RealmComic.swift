//
//  RealmComic.swift
//  Marvel
//
//  Created by Adam on 10/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import  RealmSwift


class RealmComic: Object {
    @objc  dynamic var availabe: Int = 0
    @objc private dynamic var collectionURI: String = ""
     var items: List<String> = List<String>()
    @objc  dynamic var returned: Int = 0
    
    static func newRealmChracterRootClass ( from root :Comic  ) -> RealmComic {
        let newObject = RealmComic()
        newObject.Availabel = root.available
        newObject.CollectioUri = root.collectionURI
        newObject.Items = root.items
        newObject.Returned = root.returned
        
        return newObject
    }
    
    
    var Availabel: Int {
        get{
            return availabe
        }
        set{
            safeWrite {
                availabe = newValue
            }
        }
    }
    var CollectioUri: String {
        get{
            return collectionURI
        }
        set{
            safeWrite {
                collectionURI = newValue
            }
        }
    }
    var Items: [String] {
        get{
            return Array(items)
        }
        set{
            safeWrite {
                items.append(objectsIn: newValue)
            }
        }
       
    }
    var Returned: Int {
        get{
            return availabe
        }
        set{
            safeWrite {
                returned = newValue
            }
        }
        
    }
}
