//
//  resultsTableViewController.swift
//  Marvel
//
//  Created by Adam on 10/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class resultsTableViewController: UIViewController , NVActivityIndicatorViewable , UICollectionViewDelegate , UICollectionViewDataSource {
    
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterDescription: UILabel!
    @IBOutlet weak var comicsCollectionView: UICollectionView!
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    var id : String!
    var marvelCharacter : ChracterRootClass?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let id = self.id
        print(id!)
    }
    override func viewWillAppear(_ animated: Bool) {
        let id = self.id
        getCHaracters(id: id!)
    }
    func getCHaracters(id : String ){
        
        let url = "http://gateway.marvel.com/v1/public/characters/" + id + "?ts=1&apikey=9e965f7ed63130d311a564db28b806ac&hash=f5c4c0aa5c783d02edd062a7ae2f88b3"
        WebServices.base.GetMethod(requestUrl: url) { (error, success , response ) in
            self.marvelCharacter = ChracterRootClass(json: response)
            let photo = (self.marvelCharacter?.data.results[0].thumbnail.path)! + "." + (self.marvelCharacter?.data.results[0].thumbnail.Extension)!
            WebServices.base.getPhoto(url: photo, complition: { (error , success , image ) in
                self.characterImage.image = image
            })
            self.characterName.text = self.marvelCharacter?.data.results[0].name
            self.navigationItem.title = self.marvelCharacter?.data.results[0].name
            self.characterDescription.text = self.marvelCharacter?.data.results[0].Description
            
            self.comicsCollectionView.reloadData()
            self.seriesCollectionView.reloadData()
            self.stopAnimating()
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.comicsCollectionView {
            return 5
        }
        else if collectionView == self.seriesCollectionView {
            return self.marvelCharacter?.data.results[0].series.items.count ?? 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.comicsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "comicsCell", for: indexPath) as! ComicsTableViewCell
            // Set up cell
            return cell
        }
            
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seriesCell", for: indexPath) as! SeriesTableViewCell
            // Set up cell
            return cell
        }
    }
    
    func loading(){
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "", type: NVActivityIndicatorType.lineSpinFadeLoader)
    }
    
}
